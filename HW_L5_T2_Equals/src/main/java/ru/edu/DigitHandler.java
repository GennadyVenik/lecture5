package ru.edu;

public class DigitHandler {
    private int value;

    DigitHandler(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if(!this.getClass().equals(obj.getClass())) return false;
        DigitHandler other = (DigitHandler) obj;
        return value == other.getValue();
    }
}
