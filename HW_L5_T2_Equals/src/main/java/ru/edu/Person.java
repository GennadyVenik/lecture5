package ru.edu;

import java.util.Objects;

public class Person implements Comparable<Person> {

    private String name;
    private String city;
    private int age;

    @Override
    public String toString() {
        return "Имя: " + name + ", город: " + city + ", возраст: " + age;
    }

    @Override
    public int compareTo(Person other) {
        int compareCity = city.compareTo(other.city);
        if (compareCity == 0) {
            return name.compareTo(other.name);
        } else {
            return compareCity;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city, age);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if(!this.getClass().equals(obj.getClass())) return false;
        Person other = (Person)obj;
        return (name.equalsIgnoreCase(other.name) && city.equalsIgnoreCase(other.city) && age == other.age);

    }

    public static Builder builder() {
        return new Builder();
    }


    public static class Builder {
        private Person person = new Person();

        public Builder setName(String name) {
            person.name = name;
            return this;
        }
        public Builder setCity(String city) {
            person.city = city;
            return this;
        }
        public Builder setAge(int age) {
            person.age = age;
            return this;
        }

        public Person build() {
            return person;
        }

    }
}
