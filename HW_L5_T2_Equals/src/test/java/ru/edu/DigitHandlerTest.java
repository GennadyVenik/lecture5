package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class DigitHandlerTest {

    /**
     * Проверка возвращаемого значения от {@link DigitHandler#getValue()}
     * Возвращает целочисленное значение обертки
     */
    @Test
    public void getValueTest() {
        DigitHandler digitHandler = new DigitHandler(343);
        Assert.assertEquals(343,digitHandler.getValue());
    }

    /**
     * Проверка возвращаемого значения от {@link DigitHandler#hashCode()}
     * В качестве hash значения возвращает значение поля value
     */
    @Test
    public void hashCodeTest() {
        DigitHandler digitHandler1 = new DigitHandler(343);
        DigitHandler digitHandler2 = new DigitHandler(343);
        Assert.assertEquals(digitHandler1.hashCode(), digitHandler2.hashCode());
    }

    /**
     * Проверка возвращаемого значения от {@link DigitHandler#equals(Object)}
     * DigitHandler равен другому объекту, если другой объект ссылается на тот же объект,
     * или два объекта принадлежат классу DigitHandler и их значения равны
     */
    @Test
    public void equalsTrueTest() {
        DigitHandler monkeys = new DigitHandler(12);
        DigitHandler month = new DigitHandler(12);
        Assert.assertEquals(true, monkeys.equals(monkeys));
        Assert.assertEquals(true, monkeys.equals(month));
    }

    /**
     * Проверка возвращаемого значения от {@link DigitHandler#equals(Object)}
     * DigitHandler не равен другому объекту, если другой объект null,
     * или другого типа, два объекта принадлежат классу DigitHandler и их значения не равны
     */
    @Test
    public void equalsFalseTest() {
        DigitHandler monkeys = new DigitHandler(12);
        DigitHandler samurai = new DigitHandler(1);
        Assert.assertEquals(false, monkeys.equals(null));
        Assert.assertEquals(false, monkeys.equals(new Integer(12)));
        Assert.assertEquals(false, samurai.equals(monkeys));
    }
}
