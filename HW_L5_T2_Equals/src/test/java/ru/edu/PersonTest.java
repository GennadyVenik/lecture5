package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class PersonTest {

    /**
     * Проверка возвращаемого значения от {@link Person#toString()}
     * Возвращает строковое представление данных объекта -
     * "Имя: " + name + ", город: " + city + ", возраст: " + age
     * */
    @Test
    public void toStringTest() {
        Person garry = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(22).build();
        Assert.assertEquals("Имя: Гарри, город: Хогвартс, возраст: 22", garry.toString());
    }

    /**
     * Проверка возвращаемого значения от {@link Person#hashCode()}
     * В качестве hash значения возвращает hash трех полей: name, city, age
     */
    @Test
    public void hashCodeTest() {
        Person garry1 = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(22).build();
        Person garry2 = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(22).build();
        Person garry3 = Person.builder().setName("Гарр").setCity("Хогвартс").setAge(22).build();
        Assert.assertEquals(garry1.hashCode(), garry2.hashCode());
        Assert.assertEquals(false, garry1.hashCode() == garry3.hashCode());
        Assert.assertEquals(false, garry2.hashCode() == garry3.hashCode());
    }

    /**
     * Проверка возвращаемого значения от {@link Person#equals(Object)}
     * Person равен другому объекту, если другой объект ссылается на тот же объект,
     * или два объекта принадлежат классу Person и их поля: name,city, age равны
     * name и cite без учета регистра
     */
    @Test
    public void equalsTrueTest() {
        Person garry1 = Person.builder().setName("ГАРРИ").setCity("ХогвАртс").setAge(22).build();
        Person garry2 = Person.builder().setName("гарри").setCity("ХоГвартс").setAge(22).build();
        Assert.assertEquals(true, garry1.equals(garry1));
        Assert.assertEquals(true, garry1.equals(garry2));
    }

    /**
     * Проверка возвращаемого значения от {@link Person#equals(Object)}
     * Person не равен другому объекту, если другой объект null,
     * или другого типа, два объекта принадлежат классу Person и их поля: name,city, age не равны
     * name и cite без учета регистра
     */
    @Test
    public void equalsFalseTest() {
        Person garry1 = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(22).build();
        Person garry2 = Person.builder().setName("Гарр").setCity("Хогвартс").setAge(22).build();
        Person garry3 = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(122).build();
        Assert.assertEquals(false, garry1.equals(null));
        Assert.assertEquals(false, garry1.equals(garry2));
        Assert.assertEquals(false, garry1.equals(garry3));
    }


}
