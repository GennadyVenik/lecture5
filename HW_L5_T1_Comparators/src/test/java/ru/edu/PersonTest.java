package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class PersonTest {

    /**
     * Проверка возвращаемого значения от {@link Person#toString()}
     * Возвращает строковое представление данных объекта -
     * "Имя: " + name + ", город: " + city + ", возраст: " + age
     * */
    @Test
    public void toStringTest() {
        Person garry = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(22).build();
        Assert.assertEquals("Имя: Гарри, город: Хогвартс, возраст: 22", garry.toString());
    }

    /**
     * Проверка возвращаемого значения от {@link Person#compareTo(Person)}
     * Сортирует персон (Person) сначала по городу а потом по имени
     * */
    @Test
    public void compareToTest() {
        Person garry = Person.builder().setName("Гарри").setCity("Хогвартс").setAge(22).build();
        Person tom = Person.builder().setName("Том").setCity("Хогвартс").setAge(23).build();
        Person germiona = Person.builder().setName("Гермиона").setCity("Хогвартс").setAge(21).build();
        Person sergey = Person.builder().setName("Сергей").setCity("Екб").setAge(22).build();
        Person tom2 = Person.builder().setName("Томм").setCity("Хогвартт").setAge(23).build();
        Person germi = Person.builder().setName("Герми").setCity("Омск").setAge(21).build();

        List<Person> expected = Arrays.asList(new Person[] {sergey, germi, garry, germiona, tom, tom2});
        List<Person> list = Arrays.asList(new Person[] {garry, tom, germiona, sergey, tom2, germi});
        list.sort(Person::compareTo);
        for(int i = 0; i < expected.size(); i++) {
            Assert.assertEquals(expected.toString(), list.toString());
        }

    }
}
