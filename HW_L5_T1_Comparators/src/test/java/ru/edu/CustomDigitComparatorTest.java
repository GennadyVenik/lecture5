package ru.edu;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


public class CustomDigitComparatorTest {

    /**
     * Проверка возвращаемого значения от {@link CustomDigitComparator#compare(Integer, Integer)}
     * Сортирует элементы: сначала идут четные затем не четные (в порядке возрастания)
     */
    @Test
    public void customDigitComparatorTest() {
        Integer[] expected = new Integer[] {-44, -2, 0, 2, 4, 6, 8, -3, -1, 1, 3, 7, 9, 99};
        Comparator<Integer> cmpInt = new CustomDigitComparator();
        List<Integer> list = Arrays.asList(new Integer[] {3, 4, 6, 7, 8, 9, 99, 2, 1, 0, -2, -3, -1, -44});
        list.sort(cmpInt);
        for(int i = 0; i < expected.length - 1; i++) {
            Assert.assertEquals(expected[i], list.get(i));
        }
    }


}
