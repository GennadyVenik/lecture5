package ru.edu;

public class Person implements Comparable<Person> {

    private String name;
    private String city;
    private int age;

    @Override
    public String toString() {
        return "Имя: " + name + ", город: " + city + ", возраст: " + age;
    }

    @Override
    public int compareTo(Person other) {
        int compareCity = city.compareTo(other.city);
        if (compareCity == 0) {
            return name.compareTo(other.name);
        } else {
            return compareCity;
        }
    }

    public static Builder builder() {
        return new Builder();
    }


    public static class Builder {
        private Person person = new Person();

        public Builder setName(String name) {
            person.name = name;
            return this;
        }
        public Builder setCity(String city) {
            person.city = city;
            return this;
        }
        public Builder setAge(int age) {
            person.age = age;
            return this;
        }

        public Person build() {
            return person;
        }

    }
}
