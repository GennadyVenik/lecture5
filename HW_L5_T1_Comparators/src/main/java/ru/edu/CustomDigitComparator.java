package ru.edu;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Integer> {


    /**
     * @param rhs - правый элемент
     * @param lhs - левый элемент
     * @return число отражающее правильность последовательности - 0 элементы равны, -1 lhs > rhs, 1 lhs < rhs
     * */
    @Override
    public int compare(Integer rhs, Integer lhs) {
        if(lhs == rhs) return 0;
        if(lhs % 2 == 0) {
            if(rhs % 2 == 0) {
                if(lhs > rhs) return -1;
                else return 1;
            } else return 1;
        } else {
            if(rhs % 2 == 0) {
                return -1;
            } else {
                if(lhs > rhs) return -1;
                else return 1;
            }
        }
    }
}
